﻿using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public class RolesRepository : MRepository<Role>
    {
        public RolesRepository(IMDbSettings settings)
            : base(settings, "Roles")
        {

        }
    }

}
