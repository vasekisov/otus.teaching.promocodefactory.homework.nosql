﻿using Microsoft.EntityFrameworkCore;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain;
using Otus.Teaching.Pcf.Administration.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;


namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public abstract class MRepository<T>
       : IRepository<T>
       where T : BaseEntity
    {

        private readonly IMongoCollection<T> _collection;

        public MRepository(IMDbSettings settings, string collectionName)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.Database);
            _collection = database.GetCollection<T>(collectionName);
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var entities = await _collection.Find(entity => true).ToListAsync();

            return entities;
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            var entity = await _collection.Find<T>(entity => entity.Id == id).FirstOrDefaultAsync();

            return entity;
        }

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            return await _collection.Find<T>(entity => ids.Contains(entity.Id)).ToListAsync();
        }

        public Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public Task AddAsync(T entity)
        {
            throw new NotImplementedException();
        }

        public async Task UpdateAsync(T entity)
        {
            await _collection.ReplaceOneAsync<T>(obj => obj.Id == entity.Id, entity);
        }

        public Task DeleteAsync(T entity)
        {
            throw new NotImplementedException();
        }

    }
}
