﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class MDbSettings : IMDbSettings
    {
        public string ConnectionString { get; set; }
        public string Database { get; set; }
    }
}
