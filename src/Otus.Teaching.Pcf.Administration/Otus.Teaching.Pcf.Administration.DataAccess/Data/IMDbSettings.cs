﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{    public interface IMDbSettings
    {
        string ConnectionString { get; set; }
        string Database { get; set; }
    }
}
